#!/afs/nd.edu/user14/csesoft/2017-fall/anaconda3/bin/python3

import nltk
#nltk.download('stopwords')
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
import re

def tokenize(text):
	# remove extra punctuation excluding ' and -
	text = re.sub(r"[^\w\d'\s\-]+", ' ', text)
	text = re.sub(r"\(.*\)", '', text)
	text = re.sub(r"^[0-9]+\.", '', text)
	# split by whitespace
	text = text.split()

	# remove stop words and short words
	text = [word.lower() for word in text if len(word) >= 3 and word not in stopwords.words('english')]
	
	ps = PorterStemmer()
	stemmed = [ps.stem(w) for w in text]
	return text

if __name__ == '__main__':
	data = "This is my sentence. Please remove all the common words from this string. Thank you."
	words = tokenize(data)
	print (words)
