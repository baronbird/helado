#!/afs/nd.edu/user14/csesoft/2017-fall/anaconda3/bin/python3

import sys
import block_funcs
import tf
import train

if __name__ == '__main__':
	if len(sys.argv) < 2:
		print ('usage: main.py train.in test.in')
		exit(1)
	
	first = True
	freq_counter = tf.tf()
	line_count = 0
	section_length = train.train(sys.argv[1])
	print (section_length)
	sys.exit()
	i = 0
	for line in open(sys.argv[2]):
		i += 1
		'''if line[:6] == '******':
			if first == True:
				first = False
			else:
				# normalize frequencies and find 2 most common words to print
				freq_counter.normalize(1)
				word_probs = list(freq_counter.probs.items())
				word_probs.sort(key=lambda tup: tup[1], reverse=True)
				word1 = word_probs[0][0][0].upper() + word_probs[0][0][1:]
				word2 = word_probs[1][0][0].upper() + word_probs[1][0][1:]
				word3 = word_probs[2][0][0].upper() + word_probs[2][0][1:]
				print ('{} {} {}'.format(word1, word2, word3))

				# reset counts for next chunk
				freq_counter.reset()
		else:
			block_funcs.get_tf(line, freq_counter)'''
		if line.strip() == '******':
			print (i)
			continue
		else:
			if line_count < section_length:
				block_funcs.get_tf(line, freq_counter)
				line_count += 1
			else:
				freq_counter.normalize(1)
				word_probs = list(freq_counter.probs.items())
				word_probs.sort(key=lambda tup: tup[1], reverse=True)
				word1 = word_probs[0][0][0].upper() + word_probs[0][0][1:]
				word2 = word_probs[1][0][0].upper() + word_probs[1][0][1:]
				word3 = word_probs[2][0][0].upper() + word_probs[2][0][1:]
				print ('{} {} {}'.format(word1, word2, word3))

				# reset counts for next chunk
				freq_counter.reset()
				line_count = 0

	freq_counter.normalize(1)
	word_probs = list(freq_counter.probs.items())
	word_probs.sort(key=lambda tup: tup[1], reverse=True)
	word1 = word_probs[0][0][0].upper() + word_probs[0][0][1:]
	word2 = word_probs[1][0][0].upper() + word_probs[1][0][1:]
	word3 = word_probs[2][0][0].upper() + word_probs[2][0][1:]
	print ('{} {} {}'.format(word1, word2, word3))
