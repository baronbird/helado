from nltk.tokenize import TextTilingTokenizer
import sys

if __name__ == '__main__':
	ttt = TextTilingTokenizer()
	text = ''
	for line in open(sys.argv[1]):
		if line == '******\n':
			continue
		text += line
	tokens = ttt.tokenize(text)
	for token in tokens:
		print (token)
