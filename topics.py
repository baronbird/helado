import sys
from sklearn.decomposition import LatentDirichletAllocation
from sklearn.feature_extraction.text import CountVectorizer
import operator
import collections
import preproc

def display_topics(model, feature_names, no_top_words):
	for topic_idx, topic in enumerate(model.components_):
		print ('Topic {}:'.format(topic_idx))
		print (' '.join([feature_names[i] for i in topic.argsort()[:-no_top_words-1:-1]]))

if __name__ == '__main__':
	'''Segments a given document and outputs a line of 6 asterisks where a heading should go followed by the line number of
	the first line in the corresponding section'''
	line_count = 0
	test = []
	og_test = []

	# read in training data, preprocess, and get word counts
	for line in open(sys.argv[1]):
		if line == '******\n':
			continue
		test.append(' '.join(preproc.tokenize(line.strip())))
		og_test.append(line.strip())
		line_count += 1
	tf_vectorizer = CountVectorizer(max_df=0.95, min_df=2, max_features=1000, stop_words='english')
	tf = tf_vectorizer.fit_transform(test)
	tf_feature_names = tf_vectorizer.get_feature_names()


	num_topics = min(int(line_count/12), 5) # 12 is the average section length from training data

	# initialize and perform LDA on test document
	lda = LatentDirichletAllocation(n_topics=num_topics,max_iter=70, random_state=12).fit(tf)
	#display_topics(lda, tf_feature_names, 10) # uncomment to see the top 10 words that represent each topic

# fit each line to LDA topic and find topic boundaries
last_topic = -1
headers = [1]
print ('******\t1')
for i in range(len(test)):
	lda_vals = lda.transform(tf_vectorizer.transform([test[i]]))[0]
	index, value = max(enumerate(lda_vals), key=operator.itemgetter(1))
	lda_vals2 = [lda_vals[pos] for pos in range(len(lda_vals)) if pos != index]
	index2, value2 = max(enumerate(lda_vals2), key=operator.itemgetter(1))
	if index != last_topic and value-value2 > 0.8 and value > 0.7 and i+1 != 1:
		headers.append(i+1)
		print ('******\t{}'.format(i+1))
		last_topic = index
	print (og_test[i])
#print (len(headers))
#print (headers)
