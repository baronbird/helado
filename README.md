# helado

Given a body of text from a Vatican document, predict where and what the section headings will be.

The pipeline is as follows:

dev.in -> topics.py -> generate_headlines.py

For example,

python3 topics.py dev1.in > dev1.mid   
python3 generate_headlines.py rules.out dev1.mid > my_dev1.out   
python3 eval.py dev1.out my_dev1.out
