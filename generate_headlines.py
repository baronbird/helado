#!/usr/bin/env python3
# generate section headings for some given input text using a given grammar

import sys, time
from collections import Counter
import random
import re
import nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import (
        TfidfVectorizer, TfidfTransformer, CountVectorizer
)

random.seed(time.time())

def sort_coo(coo_matrix):
    tuples = zip(coo_matrix.col, coo_matrix.data)
    return sorted(tuples, key=lambda x: (x[1], x[0]), reverse=True)

# get top tf-idf terms
def extract_topn_from_vector(feature_names, sorted_items, topn=10):
    results = {}
    for i, score in sorted_items[:topn]:
        results[feature_names[i]] = round(score, 3)

    return results


class Grammar:
    rules = {}

    def __init__(self, infile):
        for line in open(infile):
            lhs, rhs = line.split(" -> ")
            if lhs not in self.rules:
                self.rules[lhs] = []
            self.rules[lhs].append(tuple(rhs.split()))

    def generate(self, nt, rules=None):
        if nt not in self.rules:
            raise ValueError

        if rules is None:
            # generate randomly
            N = len(self.rules[nt]) - 1
            rhs = self.rules[nt][random.randint(0, N)]

            g = ''
            for sym in rhs:
                if re.match(r'[A-Z]+', sym):
                    g += self.generate(sym, rules) + " "
                else:
                    g += sym + " "
            g = g[:-1]

        else:
            # generate with rules
            # gather symbols
            rhs_dict = {}
            lhs_score = Counter()
            for lhs, w in rules.items():
                for word in w:
                    lhs_score[lhs] += word[1]
                    if lhs not in rhs_dict:
                        rhs_dict[lhs] = []
                    rhs_dict[lhs].append((word[0], word[1]))

            # give each possible rhs a score
            max_score = 0
            max_rhs = ""
            for r in self.rules.values():
                for rhs in r:
                    symcount = Counter()
                    for sym in rhs:
                        if sym in rules:
                            # only credit for symbols that can be filled with
                            # unique values
                            if symcount[sym] < len(rules[sym]):
                                symcount[sym] += 1
                    score = 0
                    for sym in symcount:
                        for i in range(symcount[sym]):
                            # weighted scoring
                            score += rules[sym][i][1]
                    if score > max_score:
                        max_score = score
                        max_rhs = rhs

            # fill in symbols with top words, if possible
            g = ''
            for sym in max_rhs:
                if sym in rules:
                    w = sorted(rules[sym], key=lambda x: x[1], reverse=True)[0]
                    if len(rules[sym]) > 1:
                        rules[sym].remove(w)
                    g += w[0] + " "
                elif re.match(r'[A-Z]+', sym):
                    g += self.generate(sym, rules=None) + " "
                else:
                    g += sym + " "
            g = g[:-1]

        return g

if __name__ == "__main__":
    # initialize grammar
    g = Grammar(sys.argv[1])

    # do tf-idf to get top words
    # gather corpus data
    test = []
    section = ''
    heading_n = []
    for line in open(sys.argv[2]):
        line = re.sub(r'\(.*\)', '', line)
        if line.startswith("*"):
            if len(line.split()) > 1:
                heading_n.append(line.strip().split()[1])
            if len(section) > 0:
                test.append(section)
            section = ''
            continue
        section += line.strip() + ' '

    # create vectorizors and transformers
    vectorizer = TfidfVectorizer()
    tfidf_transformer = TfidfTransformer(smooth_idf=True, use_idf=True)
    cv = CountVectorizer(max_df=0.85)

    word_count_vector = cv.fit_transform(test)
    tfidf_transformer.fit(word_count_vector)
    feature_names = cv.get_feature_names()
    test = [word_tokenize(sentence) for sentence in test]
    i = 0
    for section in test:
        # part-of-speech tag words
        tagged_words = nltk.pos_tag(section)
        wordtags = {}
        for w, t in tagged_words:
            wordtags[w.lower()] = t

        # remove stopwords
        section = [w for w in section if w not in stopwords.words('english')]

        # get top words by tf-idf
        section = ' '.join(section)
        tf_idf_vector = tfidf_transformer.transform(cv.transform([section]))
        sorted_items = sort_coo(tf_idf_vector.tocoo())
        keywords = extract_topn_from_vector(feature_names, sorted_items, 12)
        rules = {}
        for k in keywords:
            # add rules using part-of-speech tags
            try:
                if wordtags[k] not in rules:
                    rules[wordtags[k]] = []
                rules[wordtags[k]].append((k, keywords[k]))
            except:
                pass
        print('{}\t{}'.format(heading_n[i], g.generate("S", rules=rules).upper()))
        i += 1
