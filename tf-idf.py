import sys
import collections
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer
import preproc

class tfIdf:
	def __init__(self):
		self.vectorizer = TfidfVectorizer()

def sort_coo(coo_matrix):
	tuples = zip(coo_matrix.col, coo_matrix.data)
	return sorted(tuples, key=lambda x: (x[1], x[0]), reverse=True)

def extract_topn_from_vector(feature_names, sorted_items, topn=10):
	sorted_items = sorted_items[:topn]

	score_vals = []
	feature_vals = []

	for idx, score in sorted_items:
		score_vals.append(round(score, 3))
		feature_vals.append(feature_names[idx])

	results = {}
	for idx in range(len(feature_vals)):
		results[feature_vals[idx]] = score_vals[idx]
	
	return results

if __name__ == '__main__':
	'''		'This is the first document.',
			'This document is the second document.',
			'And this is the third one.',
			'Is this the first document?','''
	'''sentences = [
			'Lots lots lots',
			'lots the and',
			'and the the and',
			'more and the and'
			'get the makeup'
	]
	test = [
			'Lots of makeup on my face'
		]'''
	sentences = []
	chunk  = ''
	for line in open('train.in'):
		if line == '******\n':
			if len(chunk) > 0:
				sentences.append(chunk)
			chunk = ''
			continue
		chunk += line.strip() + ' '
	test = []
	chunk = ''
	for line in open('test.in'):
		if line == '******\n':
			if len(chunk) > 0:
				test.append(chunk)
			chunk = ''
			continue
		chunk += line.strip() + ' '
	corpus = [' '.join(preproc.tokenize(sentence)) for sentence in sentences]
	#corpus = sentences
	vectorizer = TfidfVectorizer()
	tfidf_transformer=TfidfTransformer(smooth_idf=True, use_idf=True)
	cv=CountVectorizer(max_df=0.85)
	word_count_vector=cv.fit_transform(sentences)
	tfidf_transformer.fit(word_count_vector)
	feature_names = cv.get_feature_names()
	test = [' '.join(preproc.tokenize(sentence)) for sentence in test]
	for section in test:
		tf_idf_vector=tfidf_transformer.transform(cv.transform([section]))

		sorted_items = sort_coo(tf_idf_vector.tocoo())

		keywords = extract_topn_from_vector(feature_names, sorted_items, 10)

		#print (test)
		print ('-----keywords-----')
		for k in keywords:
			print (k, keywords[k])
