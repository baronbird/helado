import fileinput
import nltk
from nltk.tokenize import word_tokenize
from collections import Counter

nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')

def comb(l, size):
    if size > len(l):
        raise ValueError
    comb = [" ".join(l[i:i+size]) for i in range(len(l)-size+1)]
    return comb

class Grammar:
    rules = Counter()
    s_rules = set()
    non_terms = set()
    rhs_dict = {}
    NT_LABELS = []
    s_count = 0

    def __init__(self):
        alpha = [chr(x) for x in range(ord('A'), ord('Z')+1) if chr(x) != "S"]
        self.NT_LABELS = alpha + [x + y for x in alpha for y in alpha]

    def add_rule(self, rhs, lhs=None):
        if lhs is not None:
            if lhs == "S":
                if rhs in self.rhs_dict.keys():
                    lhs = self.rhs_dict[rhs]
                else:
                    lhs = lhs + str(self.s_count)
                    self.s_count += 1
                self.s_rules.add((lhs, rhs))
            self.rhs_dict[rhs] = lhs
            self.rules["{} -> {}".format(lhs, rhs)] += 1
        else:
            if rhs not in self.rhs_dict.keys():
                lhs = self.new_nt()
                self.rhs_dict[rhs] = lhs
            else:
                lhs = self.rhs_dict[rhs]
            self.rules["{} -> {}".format(lhs, rhs)] += 1
        return self.rhs_dict[rhs]

    def new_nt(self):
        for label in self.NT_LABELS:
            if label not in self.non_terms:
                self.non_terms.add(label)
                return label

    def generate_rules(self, s):
        # add single word rules
        s = [self.add_rule(w[0], lhs=w[1]) for w in s]

        """
        # add multi-word rules
        for k in range(2, len(s)):
            for c in comb(s, k):
                self.add_rule(c)
                """

        # add full sentence rules recursively
        self.add_rule(" ".join(s), lhs="S")
        #rhs_list = self.generate_rhs(s)
        #for rhs in rhs_list:
            #self.add_rule(rhs, lhs="S")

    def generate_rhs(self, s):
        r = []
        if len(s) == 1:
            return s
        for k in range(1, len(s)):
            for t in self.generate_rhs(s[k:]):
                if k > 1:
                    r.append(self.rhs_dict[" ".join(s[:k])] + " " + t)
                else:
                    r.append(s[0] + " " + t)
        t = " ".join(s)
        if t in self.rhs_dict.keys():
            r.append(self.rhs_dict[t])
        return r

g = Grammar()

for line in fileinput.input():
    line = line.strip().lower()
    word_list = word_tokenize(line)
    tagged_words = nltk.pos_tag(word_list)
    g.generate_rules(tagged_words)

extra_s_rules = set()
for rule in g.s_rules:
    for s in rule[1].split():
        if s.startswith("S"):
            extra_s_rules.add(s)

for rule in g.s_rules:
    if rule[0] not in extra_s_rules:
        g.rhs_dict[rule[1]] = "S"
        g.rules["S -> {}".format(rule[1])] += g.rules["{} -> {}".format(rule[0], rule[1])]
        del g.rules["{} -> {}".format(rule[0], rule[1])]

for rule in extra_s_rules:
    g.rules["S -> {}".format(rule)] += 1

for r in g.rules:
    if g.rules[r] > 1:
        print(r)
