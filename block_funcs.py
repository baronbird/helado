import preproc
import tf

def get_tf(text, tf):
	words = preproc.tokenize(text)
	tf.count_words(words)

if __name__ == '__main__':
	TFcounter = tf.tf()
	train('Hello, sir. sir please', TFcounter)
	for key in TFcounter.freq:
		print ('{}: {}'.format(key, TFcounter.freq[key]))
