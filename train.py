def train(train_file):
	count = 0
	counts = []
	first = True

	for line in open(train_file):
		if line.strip() == '******':
			if first == True:
				first = False
				continue
			else:
				counts.append(count)
				count = 0
		else:
			count += 1
	return int(sum(counts)/len(counts))
