#!/afs/nd.edu/user14/csesoft/2017-fall/anaconda3/bin/python3

from nltk.translate.bleu_score import sentence_bleu, SmoothingFunction
import sys
import collections

def evaluate(actual, predicted):
	n = 0
	score = 0

	# calculate BLEU scores
	actual = list(open(actual))
	predicted = list(open(predicted))
	for prediction in predicted:
		dist = float("inf")
		g_line = int(prediction.lower().strip().split('\t')[0])
		guess = prediction.lower().strip().split('\t')[1].split()
		real = []
		for value in actual:
			r_line = int(value.lower().strip().split('\t')[0])
			v = value.lower().strip().split('\t')[1].split()
			if abs(r_line - g_line) < dist:
				real = v
				dist = abs(r_line - g_line)
		score += sentence_bleu([real], guess, smoothing_function=SmoothingFunction().method4)
		n += 1
	
	avg_score = score/n
	print('Average BLEU score: {}'.format(avg_score))

	# evaluate heading placement
	tp = 0
	actual_size = len(actual)
	brk = False
	for prediction in predicted:
		guess = int(prediction.lower().strip().split('\t')[0])
		for value in actual:
			real = int(value.lower().strip().split('\t')[0])
			if abs(real-guess) <= 2:
				tp += 1
				actual.remove(value)
				break
	
	precision = tp / len(predicted)
	recall = tp /actual_size 

	f1 = (2 * precision * recall) / (precision + recall)
	print ('precision: {}/{}'.format(tp, len(predicted)))
	print ('recall: {}/{}'.format(tp, actual_size))
	print('placement F1: {}'.format(f1))


if __name__ == '__main__':
	evaluate(sys.argv[1], sys.argv[2])
