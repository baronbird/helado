import collections

class tf:
	def __init__(self):
		self.freq = collections.Counter()
		self.total_count = 0
		self.probs = {}
	
	def count_words(self, word_list):
		"""Updates internal counter"""
		for word in word_list:
			self.freq[word] += 1
			self.total_count += 1
	
	def normalize(self, add=0):
		"""Make sure all words have probability between 0 and 1"""
		self.probs.clear()
		for key in self.freq:
			self.probs[key] = (float(self.freq[key] + add))/(self.total_count + add*len(self.freq))

	def reset(self):
		self.freq.clear()
		self.total_count = 0
		self.probs.clear()
